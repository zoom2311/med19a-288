var plz = 8100;
var x = 20;
var y = 8;
var name = "Martha";
var msg;

// Wenn die plz gleich 8100 ist, Ausgabe "Die PLZ ist "+ plz
if (plz == 8100) {
    msg = "Die PLZ ist "+ plz;
}

// Wenn die plz grösser 8000 ist, Ausgabe "Die PLZ ist grösser 8000 "+ plz
if (plz > 8000) {
    msg = "Die PLZ ist grösser 8000 "+ plz;
}

// Wenn x grösser 10 ist -> Ausgabe "10i"
// Ansonsten (Else) -> Ausgabe "5 vor 10"
if (x > 10) {
    msg = "10i";
} else {
    msg = "5 vor 10";
}

// Wenn x = 20 ist und y  = 8 ist, Ausgabe "Perfect"
if (x == 20 && y == 8) {
    msg = "Perfect";
}

// Wenn name nicht Walter ist, Ausgabe "Martha"
if (name != "Walter") {
    msg = "Martha";
}

// Wenn name Martha ist oder plz 8100 ist, Augabe "Logisch"
if (name == "Walter" || plz == 8100) {
    msg = "Logisch";
}


document.getElementById('result').innerHTML = msg;