/* Dieses Skript zeigt dem Benutzer je nach der aktuellen Uhrzeit eine andere Begrüssung an */

var today = new Date();             // Erstellt ein neues date-Objekt. 
var hourNow = today.getHours();     // Ermittelt die Stundenangabe der aktuellen Uhrzeit
var greeting; 

// Zeigt die passende Grussformel auf der Grundlage der Uhrzeit an (IF)
if (hourNow > 18) {
    greeting = "Guten Abend";
}
else if (hourNow > 12) {
    greeting = "Guten Tag";
}
else if (hourNow > 0) {
    greeting = "Guten Morgen";
}
else {
    greeting = "Willkommen";
}

document.getElementById('result').innerHTML = greeting;