/* Dieses Skript zeigt dem Benutzer je nach Uhrzeit eine andere Begrüssung an */

var today = new Date();             // Erstellt ein neues date-Objekt. 
var hourNow = today.getHours();     // Ermittelt die Stundenangabe der aktuellen Uhrzeit
var greeting;

// Zeigt die passende Grussformel auf der Grundlage der Uhrzeit an (SWITCH)
switch (hourNow) {
    case 1:
        greeting = "Gute Nacht";
        break;
    case 2:
        greeting = "Gute Nacht";
        break;  
    case 5:
        greeting = "Guten Morgen";
        break;  
    case 13:
        greeting = "Guten Tag";
        break;      
    default:
        greeting = "Willkommen";
        break;  
}

document.getElementById('result').innerHTML = greeting;