/*  Funktion createSalutation(); getCurrentDateTime();
    Gibt alle Namen im Array mit dem Grusstext zurück. -> Der Grusstext wird in einer Funktion zusammengebaut. Zudem 
    wird noch das aktuelle Datum dem Text hinzugefügt. Das aktuelle Datum wird auch über eine Funktion zusammengebaut.
*/
var names = ["Tom", "Jane", "Tim", "Jenny"];

// Funktion welche das aktuelle Datum zusammensetzt und zurückgibt
function getCurrentDateTime() {
    var now = new Date(); 
    var time = now.toLocaleTimeString(); 
    var date = now.toLocaleDateString();
    return date + " - " + time;
}

// Funktion welcher den Text zusammensetzt (Guten Tag, Tom!) und zurückgibt
function createSalutation(name) {
    var now = getCurrentDateTime();
    return now + ": Guten Tag, " + name + "!";
}

// Funktion welche die Ausgabe auf die Konsole macht.
function printText(text) {
    console.log(text);
}

// Ausgabe aller Namen des Arrays mit Grusstext (For-Schleife)
for (var i=0; i<names.length; i++) {
    var salution = createSalutation(names[i]);
    printText(salution);
}