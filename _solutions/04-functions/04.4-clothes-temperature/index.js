/*  Funktion anziehen();
    Gibt die Kleidungstücke in einer Array zurück, welche bei gewissen Temperaturen
    mitgenommen werden müssen.
*/

var temperature = 18;                                                   // Temperatur
var clothes = ["kurze Shorts", "lange Hosen", "Jacke", "T-Shirt"];      // Kleidungsstücke
var msg = "Folgende Kleidungsstücke nehme ich mit: <br />";             // Text für die Ausgabe

function anziehen(temperature) {
    let clothesTemperature = [];

    // Lange oder kurze Hosen
    if (temperature > 20) {
        clothesTemperature[0] = clothes[0];
    }
    else {
        clothesTemperature[0] = clothes[1];
    }

    // Jacke mitnehmen
    if (temperature < 15) {
        clothesTemperature[1] = clothes[2];
    }
    
    // T-Shirt kommt immer mit
    clothesTemperature[2] = clothes[3];

    return clothesTemperature;
};

clothes = anziehen(temperature);
for (i=0; i < clothes.length; i++) {

    // Wenn die Temperatur > 15 Grad ist, weiss dieser Code nicht was er in clothes[2] reinschreiben soll
    // Somit muss dieser Wert abgefangen werden.
    if (clothes[i] != undefined) {
        msg += clothes[i] +"<br>";
    } 
}

document.getElementById('result').innerHTML = msg;