/*  Funktion addNumbers();
    Gibt eine Addition zurück mit welcher 2 Zahlen (Parameter) addiert werden.
*/
function addNumbers(number1, number2) {
    var result = number1 + number2;
    return result;
}

var output = addNumbers(7, 8);
console.log("Das Resultat ist: " + output);