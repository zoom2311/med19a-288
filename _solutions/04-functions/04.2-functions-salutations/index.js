/*  Funktion createSalutation();
    Gibt alle Namen des Arrays mit dem Grusstext zurück. -> Der Grusstext wird in einer Funktion zusammengebaut.
*/

var names = ["Tom", "Jane", "Tim", "Jenny"];

// Funktion welcher den Text zusammensetzt (Guten Tag, Tom!) und zurückgibt
function createSalutation(name) {
    return "Guten Tag, " + name + "!";
}

// Funktion welche die Ausgabe auf die Konsole macht.
function printText(text) {
    console.log(text);
}

// Ausgabe aller Namen des Arrays mit Grusstext (For-Schleife)
for (var i=0; i< names.length; i++) {
    var salution = createSalutation(names[i]);
    printText(salution);
}