/* getElementById */

// Wählt das Element (id= one) aus und speichert es
// in einer Variable
var el = document.getElementById("one");

// Ändert den Wert des class-Attributs.
el.className = "cool";

/* ------------------------------------------------------------------ */
/* getElementsByClassName */

// Findet Elemente der Klasse "hot"
var elements = document.getElementsByClassName("hot");

// IF-Anweisung wenn mehr als 2 Elemente vorhanden sind
if (elements.length > 1) {   
    var el = elements[1];     // Wählt das 3. Element in der Knotenliste aus
    el.className = 'cool';    // Ändert den Wert des class-Attributs
}

/* ------------------------------------------------------------------ */
/* getElementsByTagName */

// Findet <li>-Elemente
var elements = document.getElementsByTagName('li'); 

// IF-Anweisung wenn mehr als 0 Elemente vorhanden sind
if (elements.length > 0) {            

    var el = elements[0];              // Wählt das erste Element mit der Arraysyntax aus
    el.className = "cool";             // Ändert den Wert des class-Attributs
}

/* ------------------------------------------------------------------ */
/* querySelector */

// querySelector() gibt nur die erste Übereinstimmung zurück
var el = document.querySelector("li.hot");   // Wählt das 1. Element li.hot
el.className = "cool";


/* ------------------------------------------------------------------ */
/* querySelectorAll */

// querySelectorAll gibt eine Knotenliste zurück
// Die 3. gefundenen Elemente werden ausgewählt und geändert
var els = document.querySelectorAll("li.hot");
els.className = "cool";                          // Ändert den Wert des class-Attributs


/* querySelectorAll Array */

// querySelectorAll gibt eine Knotenliste zurück
// Speichert Knotenliste in einem Array (Name array = hotItems)
var hotItems = document.querySelectorAll("li.hot");

if (hotItems.length > 0) {                     // If: Wenn es Elemente enthält
    for (var i=0; i<hotItems.length; i++) {     // Durchläuft jedes Element (For-Schleife)
        hotItems[i].className = "cool";        // Ändert Wert von class jedes Elements
    }
}

/* ------------------------------------------------------------------ */
/* previousSibling/ nextSibling */

// Wählt den Ausgangspunkt aus und findet dessen Geschwister
var startItem = document.getElementById("two");     // id = two
var prevItem = startItem.previousSibling;
var nextItem = startItem.nextSibling;

// Ändert den Wert der class-Attribute der Geschwister
prevItem.className = "complete";        // Klasse = complete
nextItem.className = "cool";            // Klasse = cool


/* ------------------------------------------------------------------ */
/* setAttribute */

var firstItem = document.getElementById("one");         //  id=one
var lastItem = document.getElementById("four");         //  id=four

// Ändert den Wert der class-Attribute (mit setAttribute)
firstItem.setAttribute("class","complete");
lastItem.setAttribute("class", "complete");

var firstItem = document.getElementById("one"); 
firstItem.setAttribute("class","cool");


/* ------------------------------------------------------------------ */
/* Value Input */
// Formularfelder mit Inhalten füllen
var firstName = document.getElementById("fname");         //  id=fname
var lastName = document.getElementById("lname");         //   id=lname

// Ändert den Wert der value-Attributs (mit set Attribute)
firstName.setAttribute("value","Stefan");
lastName.setAttribute("value", "Jenni");

//oder direkt mit .value
firstName.value = "Stefan";
lastName.value = "Jenni";

// Ausgabe von Vor- und Nachname in der Konsole
console.log(firstName.value);   // Ausgabe Stefan
console.log(lastName.value);    // Ausgabe Jenni

var firstName = document.getElementById("fname");         //  id=fname
var lastName = document.getElementById("lname");         //  id=lname

// If Abfrage Errormeldung in id=errorVorname wenn Vorname keinen Wert hat
if(firstName.value == "") {
    var errorVorname = document.getElementById("errorVorname");
    errorVorname.textContent = "Bitte geben Sie Ihren Vornamen ein";
}

// If Abfrage Errormeldung in id=errorNachname wenn Nachname keinen Wert hat
if(lastName.value == "") {
    var errorNachname = document.getElementById("errorNachname");
    errorNachname.textContent = "Bitte geben Sie Ihren Nachnamen ein";
}