var scores = [24, 32, 17];      // Punktezahlen Array
var arrayLength = scores.length;// Elemente im Array
//var roundNumber = 0;            // Aktuelle Runde
var msg = '';                   // Meldung
var i;                          // Zähler

// Durchläuft die Elemente im Array
for (i = 0; i < arrayLength; i++) {
  
  // Mit let lässt sich eine Variable erzeugen, die nur innerhalb der For-Schleife ihre Gültigkeit hat (Scope)
  let roundNumber = 0;

  // Zählung in Arrays beginnt mit 0 (0 ist Runde 1)
  // Addiert 1 zur akteullen Rundennummer
  roundNumber = (i + 1);  // roundNumber++; würde auch funktionieren.

  // Schreibt die akteulle Rundennummer in die Meldung
  msg += "Round " +  roundNumber + ": ";

  // Ruft die Punktezahl vom Array scores ab
  msg += scores[i] + "<br/>";

}
document.getElementById('result').innerHTML = msg;