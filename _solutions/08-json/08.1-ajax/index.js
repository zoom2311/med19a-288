var xhr = new XMLHttpRequest(); // XMLHttpRequest-Objekt erstellen

xhr.onload = function() { // Wenn sich der Readystate ändert
    if (xhr.status === 200) { // Wenn der Serverstatus in Ordnung war
        var responseObject = JSON.parse(xhr.responseText);
        // STRING MIT NEUEM INHALT AUFBAUEN (könnte auch DOM-Manipulation verwenden)
        var newContent = '';
        for (var i = 0; i < responseObject.events.length; i++) { // Objekt durchlaufen
            newContent += '<div class = "event">';
            newContent += '<img src = "' + responseObject.events[i].map + '"';
            newContent += 'alt = "' + responseObject.events[i].location + '" />';
            newContent += '<p> <b>' + responseObject.events[i].location + '</b> <br>';
            newContent += responseObject.events[i].date + '</p>';
            newContent += '</div>';
        }
    } else {
        newContent = '<h5>Daten konnten nicht geladen werden</h5>';
    }

    // Aktualisiere die Seite mit dem neuen Inhalt
    document.getElementById('result').innerHTML = newContent;

};

xhr.open('GET', 'data/events.json', true); // Bereite die Anfrage vor
xhr.send(null); // Anfrage senden