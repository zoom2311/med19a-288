var nofClicks = 0; // Variable Anzahl Klicks

// Funktion Ausgabe Text im Alert
function outputAlert() {
    nofClicks++;
    alert("Sie haben nun bereits " + nofClicks + "mal auf den Button geklickt.");
}

// Eventlistener
document.getElementById("btn").addEventListener("click", outputAlert, false);