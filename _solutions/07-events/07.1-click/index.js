function outputAlert() {
    alert("Hallo!");
}
// Eventlistener
document.getElementById("btn").addEventListener("click", outputAlert, false);

// oder mit herkömmlichem DOM Eventhandler
document.getElementById('btn').onclick = function() {
    alert("Hallo!");
}

// oder mit einer anonymen Funktion
document.getElementById("btn").addEventListener("click", function() {
    alert("Hallo!");
}, false);