var nofClicks = 0; // Variable Anzahl Klicks

// Funktion Ausgabe im div msg-box
function outputText(text) {
    var outputField = document.getElementById('msg-box');
    outputField.innerText = text;
}

// Btn Click-Event
document.getElementById('btn').addEventListener("click", function() {
        nofClicks++;
        outputText("Sie haben nun bereits " + nofClicks + "mal auf den Button geklickt.");
    },
    false);

// Btn-reset Click Event
document.getElementById('btn-reset').addEventListener("click", function() {
    nofClicks = 0;
    outputText("-");
}, false);