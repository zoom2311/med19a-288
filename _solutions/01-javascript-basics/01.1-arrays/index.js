/* --------- Arrays */

// Erstellen Sie ein Array (colors) welches Ihre 3 Lieblingsfarben speichert
var colors = ["red", "blue", "black"];

// Geben Sie den Inhalt des zweiten Elements im Ergebnis aus.
document.getElementById("result").innerHTML = colors[1];

// Speichern Sie den Inhalt des ersten Elements in einer Variable (favoriteColor)
var favoriteColor = colors[0]; 

// Geben Sie die Variable favoriteColor im Ergebnis aus.
document.getElementById("result").innerHTML = favoriteColor;

// Erstellen Sie ein Array (weekDays) welches die Namen der Wochentage speichert
var weekDays = ["Montag", "Dienstag", "Mittwoch", "Donnerstag","Freitag", "Samstag","Sonntag"];

// Speichern sie die Anzahl der Wochentage in eine Variable (count) -> Verwenden Sie dazu die .length-Funktion
var count = weekDays.length;

// Geben Sie die Anzahl der Wochentage im Ergebnis aus und fügen sie den String "Tage" hinzu. -> Ausgabeergebnis soll "7 Tage" sein
document.getElementById("result").innerHTML = count + " Tage";

// Überschreiben sie den Wert (Samstag) im Array weekDays mit "saturday";
weekDays[6] = "saturday";

// Geben Sie die saturday im Array im Ergebnis aus.
document.getElementById("result").innerHTML = weekDays[6];

// Fügen Sie dem Array colors eine neue Farbe hinzu (Verwenden sie die push-Funktion)
colors.push("green");

// Geben Sie die neue Farbe im Array im Ergebnis aus.
document.getElementById("result").innerHTML = colors[3];

// Entfernen Sie Ihre die zuletzt erstellte Farbe aus dem Array colors (Verwenden sie die pop-Funktion)
colors.pop();

// Fügen Sie am Anfang des Arrays colors eine neue Farbe hinzu (Verwenden sie die unshift-Funktion)
colors.unshift("white");

// Geben Sie die neue Farbe im im Ergebnis aus.
document.getElementById("result").innerHTML = colors[0];

// Entfernen Sie Ihre die zuletzt erstellte Farbe aus dem Array colors (Verwenden sie die shift-Funktion)
colors.shift();

// Geben Sie die neue erste Farbe im Array im Ergebnis aus.
document.getElementById("result").innerHTML = colors[0];