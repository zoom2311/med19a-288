/* --------- Strings */

// Definieren Sie eine Variable firstName und weisen Sie Ihren Vornamen zu. 
// Definieren Sie eine zweite Variable lastName und weisen Sie Ihren Nachnamen zu. 
var firstName = "Stefan";
var lastName = "Jenni";

// Geben Sie die Variable "firstname" im Ergebnis aus.
document.getElementById('result').innerHTML = firstName;

// Schreiben Sie den Code so, dass der Vorname + Nachname mit Hilfe der Variablen ausgegeben wird. (Bsp: Max Meier)
document.getElementById('result').innerHTML = firstName + " " + lastName;

//Initieren Sie eine Konstante name und weisen Sie ihr einen fixen String zu
const name = "Afrika";

// Geben Sie die Konstante "name" im Ergebnis aus.
document.getElementById('result').innerHTML = name;

/* --------- Number */

// Definieren Sie eine Variable price1 und weisen sie eine Ganzzahl zu.
// Definieren Sie eine Variable price2 und weisen sie eine Ganzzahl zu. 
var price1 = 4;
var price2 = 5;

//Addieren Sie die beiden Werte und speichern Sie in einer neuen Variable "total"
var total = price1 + price2;

//Geben Sie die Variabel "total" im Ergebnis aus.
document.getElementById('result').innerHTML =  total;

// Fügen Sie dem Ergebnis noch einen String hinzu "CHF";
document.getElementById('result').innerHTML =  total + " CHF";


/* --------- Float */

// Definieren Sie eine Variable price3 und weisen sie eine Fliesskommazahl zu.
// Definieren Sie eine Variable price4 und weisen sie eine Fliesskommazahl zu. 
// In Kurzschreibweise
var price3 = 4.5, price4 = 5.3;

//Multiplizieren sie die beiden Werte und speichern Sie in der Variable "total"
var total = price3 * price4;

//Geben Sie die Variable "total" im Ergebnis aus.
document.getElementById('result').innerHTML =  total;

// Runden Sie das Ergebnis auf eine Ganzzahl
document.getElementById('result').innerHTML =  Math.round(total);

/* --------- Boolean */
// Definieren Sie eine Variable "yes" und weisen sie den Wert true zu.
// Definieren Sie eine Variable "no" und weisen sie den Wert false zu.
var yes = true;
var no = false;

//Geben Sie die Variable "yes" im Ergebnis aus.
document.getElementById('result').innerHTML =  yes;