// Erstellen Sie (3 Varablen) für die Grussformel (Name, Vorname, Text)
var firstName = 'Molly ';
var lastName = 'Kaufmann';
var message = ', bitte überprüfen Sie Ihre Bestellung:';
// Verketten Sie die 3 Variablen zu einer Grussformel
var welcome = "Hallo " + firstName + lastName + message;

// Array (clothes) erstellen mit einigen Kleidungsstücken
var clothes = ["Pullover", "Hose", "T-Shirt", "Jacke"];

// Variablen erstellen für die Ausgabe in der Preistabelle
// product : aus Array entnehmen
// amount: Zahl
// pricePullover: Zahl
// subTotal: amount *  price
// shipping: Versandkosten
// grandTotal: Gesamtbetrag
var product = clothes[0];
var amount = 4;
var pricePullover = 20
var subTotal = pricePullover * amount;
var shipping = 7;
var grandTotal = subTotal + shipping;

// Ruft das Element mit dem Id-Wert greeting ab
var el = document.getElementById('greeting');
// Ersetzt den Inhalt des Elements durch den personalisierten Gruss
el.textContent = welcome;

// Ruft das Element mit dem Id-Wert userProduct ab
var elProduct = document.getElementById('userProduct');
// Ersetzt den Inhalt des Elements durch den Produktname aus dem Array
elProduct.textContent = product;

// Ruft das Element mit dem Id-Wert userAmount ab
var elAmount = document.getElementById('userAmount');
// Ersetzt den Inhalt des Elements durch die Anzahl der bestellten Produkte (amount)
elAmount.textContent = amount;

// Ruft das Element mit dem Id-Wert subTotal ab
var elSubTotal = document.getElementById('subTotal');
// Ersetzt den Inhalt des Elements durch das Subtotal (subTotal) und fügt den String "CHF" hinzu
elSubTotal.textContent = 'CHF ' + subTotal;

// Ruft das Element mit dem Id-Wert shipping ab
var elShipping = document.getElementById('shipping');
// Ersetzt den Inhalt des Elements durch die Versandkosten (shipping) und fügt den String "CHF" hinzu
elShipping.textContent = 'CHF ' + shipping;

// Ruft das Element mit dem Id-Wert grandTotal ab
var elGrandTotal = document.getElementById('grandTotal');

// Ersetzt den Inhalt des Elements durch die Gesamtkosten und fügt den String "CHF" hinzu
elGrandTotal.textContent = 'CHF ' + grandTotal;