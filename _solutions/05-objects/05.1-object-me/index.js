/* Erstellt ein Objekt "me" mit den Personalien firstName, lastName, age */

var me = {
    firstName: "Hans",
    lastName: "Jung",
    age: "99"
}

document.getElementById('firstname-cell').innerHTML = me.firstName;
document.getElementById('lastname-cell').innerHTML = me.lastName;
document.getElementById('age-cell').innerHTML = parseInt(me.age);
