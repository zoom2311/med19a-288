/* --------- MAP OBJEKT --------- */

/* Erstellt ein Map-Objekt person von Luke Skywalter mit firstName, lastName, occupation. */
var personMap = new Map([
  ["firstName", "Luke"],
  ["lastName", "Skywalker"],
  ["occupation", "Jedi Knight"],
  [1, "pilot"]
]);

// Ein Attribut (movie, Star Wars) der Map hinzufügen
personMap.set("movie", "Star Wars"); // neues Feld setzen

// Objekt let john = { name: "John" } als Key einfügen
let john = { name: "John" };
personMap.set(john, 123);

// Ein Attribut auslesen und in der Konsole ausgeben
personMap.get("firstName"); //Luke

// Check ob ein Key in der Map existiert
personMap.has('shark'); // false  -> Shark?
personMap.has('firstName'); // true -> firstName ?

// Anzahl Elemente in der Map
personMap.size // 5

// Entfernt ein Attribut von der Map mit dem Keynamen
personMap.delete('lastName');

// Entleert die Map
personMap.clear();

// Ausgabe aller Values einer Map mit einer ForEach Schleife (Achtung die Zeile davor - Leeren der Map muss auskommentiert sein)
personMap.forEach( (value, key) => {
  console.log("Key:" + key + ": Value: "+ value);
});

// Ausgabe aller Values einer Map mit einer For-Of Schleife
for (const [key, value] of personMap) {
  console.log("Key:" + key + ": Value: "+ value);
} 


/* --------- SET OBJEKT --------- */

/* Erstellt ein Set-Objekt colors mit 3 Farben. */
var colors = new Set(
[ "orange",
  "red",
  "blue"
]);

// Objekt dem Set hinzufügen
let peter = { name: "Peter" };
let franz = { name: "Franz" };
colors.add(peter);
colors.add(franz);

// Objekt 2 mal hinzufügen (Um zu testen, ob das Set wirklich nur 1mal den Wert speichert)
colors.add(peter); // Wird nicht mehr hinzugefügt

// Anzahl Elemente im Set
colors.size;  // 5

// Check ob ein Wert im Set existiert
colors.has("orange"); // true 

//for of Set Ausgabe in Konsole
for (let value of colors) {
  console.log("Value: " + value);
} 

//forEach Ausgabe in Konsole
colors.forEach((value) => {
  console.log("Value: " + value);
});