/* Erstellt eine Template des Objekts Auto */
function Auto (marke, typ, leistung, leergewicht, image, link) {
    this.marke = marke;
    this.typ = typ;
    this.leistung = leistung;
    this.leergewicht = leergewicht;
    this.image = image;
    this.link = link;
};

/* Erstellt eine Funktion welche die Ausgabe als HTML aufbereitet und gibt den Sting zurück */
function createAutoHTML(autoObjekt) {

    let htmlTeaser = "";

    htmlTeaser += "<div class='col-12 col-md-4 mb-2 teaser-auto'><div class='row'><div class='col pl-0'><div class='teaser-img teaser-stick'>";
    htmlTeaser += "<img src='" + autoObjekt.image + "' alt='"+ autoObjekt.marke + " " + autoObjekt.typ + "' class='img-fluid z-1 mr-1' />";
    htmlTeaser +=  "<div class='teaser-overlay pt-2 pl-4 z-2'><div class='teaser-txt'><h2 class='mb-3'></h2><p class='mb-4'>";
    htmlTeaser += "<span class='bold clearfix'>"+ autoObjekt.typ +"</span>";
    htmlTeaser +=  "<table><tr><td>Typ:</td><td>" + autoObjekt.typ+  "</td></tr><tr><td>PS:</td><td>"+ autoObjekt.leistung +"</td></tr><tr><td>Leergewicht:</td><td>" + autoObjekt.leergewicht + "</td></tr></table></p>";
    htmlTeaser += "<a target='_blank' class='teaser' href='"+ autoObjekt.link +"'>Mehr erfahren</a> ";
    htmlTeaser += "</div></div></div></div></div></div>";

    return htmlTeaser;
}

// Erstellt einen BMW
var bmw = new Auto("BWM", "M5", 380, "1430 kg", "img/auto-1.jpg", "https://de.wikipedia.org/wiki/BMW_M5");
console.log(bmw.marke);

// Erstellt einen AUDI
var audi = new Auto("AUDI", "RS4", 580, "1830 kg", "img/auto-2.jpg", "https://de.wikipedia.org/wiki/Audi_RS4");

// Erstellt einen FERRARI
var ferrari = new Auto("FERRARI", "F8 Spider", 720, "1220 kg", "img/auto-3.jpg", "https://de.wikipedia.org/wiki/Ferrari_F8");

document.getElementById('result').innerHTML = createAutoHTML() + createAutoHTML(audi) + createAutoHTML(ferrari);