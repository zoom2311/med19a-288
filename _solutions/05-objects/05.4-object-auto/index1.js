/* 
    Erstellt ein Objekt "auto" mit den Eigenschaften marke, leistung, leergewicht, link, img
    Ausgabe als HTML-Konstrukt im DIV id= result
*/
var auto = {
    marke: "BMW",
    typ: "M5",
    leistung: 380,
    leergewicht: "1430 kg",
    image: "img/auto-1.jpg",
    link: "https://de.wikipedia.org/wiki/BMW_M5"
}

var htmlTeaser = "";
htmlTeaser += "<h1 class='title'>"+ auto.marke + "</h1"; 
hmmlTeaser += "<h2 class='title2'>" + auto.typ + "<h2>";

htmlTeaser += "<div class='col-12 col-md-4 mb-2 teaser-auto'><div class='row'><div class='col pl-0'><div class='teaser-img teaser-stick'>";
htmlTeaser += "<img src='" + auto.image + "' alt='"+ auto.marke + " " + auto.typ + "' class='img-fluid z-1 mr-1' />";
htmlTeaser +=  "<div class='teaser-overlay pt-2 pl-4 z-2'><div class='teaser-txt'><h2 class='mb-3'></h2><p class='mb-4'>";
htmlTeaser += "<span class='bold clearfix'>"+ auto.typ +"</span>";
htmlTeaser +=  "<table><tr><td>Typ:</td><td>" + auto.typ+  "</td></tr><tr><td>PS:</td><td>"+ auto.leistung +"</td></tr><tr><td>Leergewicht:</td><td>" + auto.leergewicht + "</td></tr></table>";
htmlTeaser += "<a target='_blank' class='teaser' href='"+ auto.link +"'>Mehr erfahren</a> ";
htmlTeaser += "</div></div></div></div></div></div>";


document.getElementById('result').innerHTML = htmlTeaser;