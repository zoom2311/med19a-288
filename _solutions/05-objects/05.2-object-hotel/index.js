/* Erstellt ein Objekt hotel mit name, rooms, booked. Dazu sollen alle Attribute mittels einer For-In Schleife ausgegebaen werden */
var msg = "";

// Objekt Hotel
var hotel = {
    name: "Einstein",
    rooms: 120,
    booked: 77
}

// Ausgabe aller Hotelattribute
for (let prop in hotel) {
    msg += "Attribut: " + prop + " = " + hotel[prop] + "<br />";
  }

document.getElementById('result').innerHTML = msg;