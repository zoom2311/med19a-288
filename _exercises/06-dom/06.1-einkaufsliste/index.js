/* getElementById */

// Wählt das Element (id= one) aus und speichert es
// in einer Variable


// Ändert den Wert des class-Attributs.


/* ------------------------------------------------------------------ */
/* getElementsByClassName */

// Findet Elemente der Klasse "hot"


// IF-Anweisung wenn mehr als 2 Elemente vorhanden sind
   
                        // Wählt das 3. Element in der Knotenliste aus
                        // Ändert den Wert des class-Attributs


/* ------------------------------------------------------------------ */
/* getElementsByTagName */

// Findet <li>-Elemente


// IF-Anweisung wenn mehr als 0 Elemente vorhanden sind
          

                        // Wählt das erste Element mit der Arraysyntax aus
                        // Ändert den Wert des class-Attributs


/* ------------------------------------------------------------------ */
/* querySelector */

// querySelector() gibt nur die erste Übereinstimmung zurück
                       // Wählt das 1. Element li.hot



/* ------------------------------------------------------------------ */
/* querySelectorAll */

// querySelectorAll gibt eine Knotenliste zurück
// Die 3. gefundenen Elemente werden ausgewählt und geändert

                        // Ändert den Wert des class-Attributs


/* querySelectorAll Array */

// querySelectorAll gibt eine Knotenliste zurück
// Speichert Knotenliste in einem Array (Name array = hotItems)


                        // If: Wenn es Elemente enthält
                        // Durchläuft jedes Element (For-Schleife)
                        // Ändert Wert von class jedes Elements
    


/* ------------------------------------------------------------------ */
/* previousSibling/ nextSibling */

// Wählt den Ausgangspunkt aus und findet dessen Geschwister
                        // id = two


// Ändert den Wert der class-Attribute der Geschwister
                       // Klasse = complete
                       // Klasse = cool


/* ------------------------------------------------------------------ */
/* setAttribute */

                      //  id=one
                      //  id=four

// Ändert den Wert der class-Attribute (mit setAttribute)


/* ------------------------------------------------------------------ */
/* Value Input */
// Formularfelder mit Inhalten füllen
                    //  id=fname
                    //   id=lname

// Ändert den Wert der value-Attributs (mit set Attribute)


//oder direkt mit .value


// Ausgabe von Vor- und Nachname in der Konsole
                    // Ausgabe Stefan
                    // Ausgabe Jenni

                    //  id=fname
                    //  id=lname

// If Abfrage Errormeldung in id=errorVorname wenn Vorname keinen Wert hat


// If Abfrage Errormeldung in id=errorNachname wenn Nachname keinen Wert hat
