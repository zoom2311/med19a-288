function versteckeFehler(){
    // damit die Fehler ausgeblendet werden, muss das div id=fehler ausgeblendet und der Text entfernt werden.
    var fehlerContainer = document.getElementById("fehler");
    fehlerContainer.style.display = "none";
    fehlerContainer.innerHTML = "";
}

function schreibeFehler(meldung){
    //diese Funktion hilft, Fehlermeldungen auszugeben
}

function pruefeVornamen(){
    //pruefe, ob ein Name eingegeben wurde und ob der name nicht Max ist - Fall der Vorname Max eingegeben wird, soll eine Fehlermeldung ("Die Anmeldung für Max wurde gesperrt") ausgegeben werden.

}

function pruefeNachnamen(){
    //pruefe, ob ein Nachname eingegeben wurde und ob er länger als ein Buchstabe ist
   
}

function pruefeEmail(){
    //pruefe, ob eine Emailadresse eingegeben wurde und ob sie ein @ enthält. Das ist zwar keine ausreichende Prüfung, aber im Moment gibt es nicht mehrs
  
}

function checkForm(){
    versteckeFehler();   // damit die Fehler ausgeblendet werden, muss das div id=fehler ausgeblendet und der Text entfernt werden.
    pruefeVornamen();
    pruefeNachnamen();
    pruefeEmail();
}