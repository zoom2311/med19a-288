/*  Funktion anziehen();
    Gibt die Kleidungstücke in einer Array zurück, welche bei gewissen Temperaturen
    mitgenommen werden müssen.
*/

var temperature = 18;                                                   // Temperatur
var clothes = ["kurze Shorts", "lange Hosen", "Jacke", "T-Shirt"];      // Kleidungsstücke
var msg = "Folgende Kleidungsstücke nehme ich mit: <br />";             // Text für die Ausgabe

// Ausgabe Kleidungstücke
document.getElementById('result').innerHTML = msg;