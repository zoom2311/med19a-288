/* Dieses Skript zeigt dem Benutzer je nach der aktuellen Uhrzeit eine andere Begrüssung an */

var today = new Date();             // Erstellt ein neues date-Objekt. 
var hourNow = today.getHours();     // Ermittelt die Stundenangabe der aktuellen Uhrzeit
var greeting;