var requirements = document.getElementById('requirements')
requirements.style.overflow = 'hidden';
var requirementsToggler = document.getElementById('requirementsToggler');
var requirementsMode = sessionStorage.getItem('requirementsMode') ? sessionStorage.getItem('requirementsMode'): 'visible';
handlerequirementsMode(requirementsMode);
requirementsToggler.addEventListener('click', function() {
    requirementsMode = requirementsMode == 'visible' ? 'hidden': 'visible';
    handlerequirementsMode(requirementsMode);
})
function handlerequirementsMode(requirementsMode) {
    requirements.style.display = requirementsMode == 'visible'? 'inherit': 'none';
    requirementsToggler.innerText = 'Anforderungsbeschreibung ' + (requirementsMode =='visible'? 'ausblenden': "einblenden");
    sessionStorage.setItem('requirementsMode', requirementsMode);
}