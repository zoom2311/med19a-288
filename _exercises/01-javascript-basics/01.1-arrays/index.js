/* --------- Arrays */

// Erstellen Sie ein Array (colors) welches Ihre 3 Lieblingsfarben speichert

// Geben Sie den Inhalt des zweiten Elements im Ergebnis aus.

// Speichern Sie den Inhalt des ersten Elements in einer Variable (favoriteColor)

// Geben Sie die Variable favoriteColor im Ergebnis aus.

// Erstellen Sie ein Array (weekDays) welches die Namen der Wochentage speichert

// Speichern sie die Anzahl der Wochentage in eine Variable (count) -> Verwenden Sie dazu die .length-Funktion

// Geben Sie die Anzahl der Wochentage im Ergebnis aus und fügen sie den String "Tage" hinzu. -> Ausgabeergebnis soll "7 Tage" sein

// Überschreiben sie den Wert (Samstag) im Array weekDays mit "saturday";

// Geben Sie die saturday im Array im Ergebnis aus.

// Fügen Sie dem Array colors eine neue Farbe hinzu (Verwenden sie die push-Funktion)

// Geben Sie die neue Farbe im Array im Ergebnis aus.

// Entfernen Sie Ihre die zuletzt erstellte Farbe aus dem Array colors (Verwenden sie die pop-Funktion)

// Fügen Sie am Anfang des Arrays colors eine neue Farbe hinzu (Verwenden sie die unshift-Funktion)

// Geben Sie die neue Farbe im im Ergebnis aus.

// Entfernen Sie Ihre die zuletzt erstellte Farbe aus dem Array colors (Verwenden sie die shift-Funktion)

// Geben Sie die neue erste Farbe des Arrays im Ergebnis aus.

// Verwenden Sie eine For-Schleife und geben sie alle Farben in der Konsole aus.