/* --------- Strings */

// Definieren Sie eine Variable firstName und weisen Sie Ihren Vornamen zu. 
// Definieren Sie eine zweite Variable lastName und weisen Sie Ihren Nachnamen zu. 

// Geben Sie die Variable "firstname" im Ergebnis aus.

// Schreiben Sie den Code so, dass der Vorname + Nachname mit Hilfe der Variablen ausgegeben wird. (Bsp: Max Meier)

// Initieren Sie eine Konstante name und weisen Sie ihr einen fixen String zu

// Geben Sie die Konstante "name" im Ergebnis aus.

/* --------- Number */

// Definieren Sie eine Variable price1 und weisen sie eine Ganzzahl zu.
// Definieren Sie eine Variable price2 und weisen sie eine Ganzzahl zu. 

//Addieren Sie die beiden Werte und speichern Sie in einer neuen Variable "total"

//Geben Sie die Variabel "total" im Ergebnis aus.

// Fügen Sie dem Ergebnis noch einen String hinzu "CHF";

/* --------- Float */

// Definieren Sie eine Variable price3 und weisen sie eine Fliesskommazahl zu.
// Definieren Sie eine Variable price4 und weisen sie eine Fliesskommazahl zu. 
// In Kurzschreibweise

//Multiplizieren sie die beiden Werte und speichern Sie in der Variable "total"

//Geben Sie die Variable "total" im Ergebnis aus.

// Runden Sie das Ergebnis auf eine Ganzzahl

/* --------- Boolean */
// Definieren Sie eine Variable "yes" und weisen sie den Wert true zu.
// Definieren Sie eine Variable "no" und weisen sie den Wert false zu.

//Geben Sie die Variable "yes" im Ergebnis aus.