/* !!! Dieses JavaScript nicht verändern !!! */


$(function() { // When the DOM is ready

    // CLICK ON PRIMARY NAVIGATION
    $('nav a').on('click', function(e) { // Click on nav
        e.preventDefault(); // Prevent loading
        var url = this.href; // Get URL to load

        $('nav a.current').removeClass('current'); // Update nav
        $(this).addClass('current');

        $('#container').remove(); // Remove old part
        $('#content').load(url + ' #container', function() {

            // Load dynamic JavaScript per page
            var className = $("#container").attr('class');
            if (className != "none") {
                const script = document.createElement('script')
                script.src = 'js/' + className + '.js';
                document.head.append(script);
            }

        }).hide().fadeIn('slow'); // Add new
    });

});